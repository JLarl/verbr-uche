import java.sql.Date;

public class Verbrauch {
	private Date d;
	private double wert;
	public Verbrauch(Date d, double wert) {
		this.d = d;
		this.wert = wert;
	}
	public Date getD() {
		return d;
	}
	public void setD(Date d) {
		this.d = d;
	}
	public double getWert() {
		return wert;
	}
	public void setWert(double wert) {
		this.wert = wert;
	}
}

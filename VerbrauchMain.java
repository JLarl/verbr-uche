import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class VerbrauchMain {

	//Konnte das Programm nicht testen, da ich einen Fehler bei Sqlite habe.
	
	private static Connection con;
	static ArrayList<Verbrauch> arr1= new ArrayList<Verbrauch>();
	static ArrayList<Verbrauch> arr2= new ArrayList<Verbrauch>();
	
	VerbrauchMain()throws SQLException{
		try{
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:C:/sqlite-tools-win32-x86-3150100/Alpenverein.db");
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws SQLException {
		createInterpolierte();
		getVerbrauch();
		insertiVerbrauch();
		show();
	}

	public static void createInterpolierte() throws SQLException
	{
		PreparedStatement stmt;
		String s1=	"CREATE TABLE IF NOT EXISTS i_strom_verbrauch("+
					"Wert REAL NOT NULL);";
		stmt=con.prepareStatement(s1);
		stmt.executeUpdate();
		String s2=	"CREATE TABLE IF NOT EXISTS i_wasser_verbrauch("+
					"Wert REAL NOT NULL);";
		stmt=con.prepareStatement(s2);
		stmt.executeUpdate();
		stmt.close();
	}
	
	public static void getVerbrauch() throws SQLException
	{
		int i=0;
		int j=0;
		String s1 = "SELECT * FROM wasser_verbrauch;";
		Statement stmt = con.createStatement();
		ResultSet rs1 = stmt.executeQuery(s1);
		try
		{
			while(rs1.next())
			{
				arr1.add(i,(new Verbrauch(rs1.getDate(1), rs1.getDouble(2))));
				i++;
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		String s2 = "SELECT * FROM strom_verbrauch;";
		ResultSet rs2 = stmt.executeQuery(s2);
		try
		{
			while(rs2.next())
			{
				arr1.add(j,(new Verbrauch(rs2.getDate(1), rs2.getDouble(2))));
				j++;
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		rs1.close();
		rs2.close();
		stmt.close();
	}
	
	private static double getiVerbrauch(Verbrauch v1, Verbrauch v2)
	{
		double x=(v2.getWert()-v1.getWert())/(v2.getD().getTime()-v1.getD().getTime());
		double y=v2.getWert()-v2.getD().getTime()*x;
		return x+y;
	}
	
	public static void insertiVerbrauch()
	{
		double k;
		PreparedStatement stmt=null;
		String s;
		try{
			for(int q= 0; q<(arr1.size()-1);q++)
			{
				s="INSERT INTO i_wasser_verbrauch VALUES(?)";
				stmt=con.prepareStatement(s);
				k=getiVerbrauch(arr1.get(q),arr1.get(q+1));
				stmt.setDouble(1, k);
				stmt.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		try{
			for(int q= 0; q<(arr2.size()-1);q++)
			{
				s="INSERT INTO i_strom_verbrauch VALUES(?)";
				stmt=con.prepareStatement(s);
				k=getiVerbrauch(arr2.get(q),arr2.get(q+1));
				stmt.setDouble(1, k);
				stmt.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void show() throws SQLException
	{
		double d1=0;
		double d2=0;
		String s1 = "SELECT value FROM wasser_verbrauch;";
		String s2 = "SELECT value FROM strom_verbrauch;";
		Statement stmt = con.createStatement();
		ResultSet rs1 = stmt.executeQuery(s1);
		try
		{
			while(rs1.next())
			{
				System.out.println("Wasserverbrauchswert: " + rs1.getDouble(1));
				d1=d1+rs1.getDouble(1);
			}
			System.out.println("Wasserverbrauchsdurchschnitt: " +d1);
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		ResultSet rs2 = stmt.executeQuery(s2);
		try
		{
			while(rs2.next())
			{
				System.out.println("Stromverbrauchswert: " + rs2.getDouble(1));
				d2=d2+rs2.getDouble(1);
			}
			System.out.println("Stromverbrauchsdurchschnitt: " +d2);
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
